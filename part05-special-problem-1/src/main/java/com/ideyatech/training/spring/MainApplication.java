package com.ideyatech.training.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        //TODO 7. read a text file as a classpath resource that contains the bean name of the CoffeeStore e.g. frenchCoffeeStore
        //TODO 8. Based on the bean name, use the application context to retrieve the CoffeeStore bean
        //TODO 9. call the CoffeeStore buy method
    }
}
