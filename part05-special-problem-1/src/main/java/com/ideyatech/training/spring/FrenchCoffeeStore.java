package com.ideyatech.training.spring;

import com.ideyatech.training.spring.xmlconfig.CoffeeStore;

/**
 * Created by USER on 5/29/2017.
 */
//TODO 3. Create an implementation for FrenchCoffeeStore
public interface FrenchCoffeeStore extends CoffeeStore {
    //TODO 4. Read a message property with French locale, property name is coffee
}
