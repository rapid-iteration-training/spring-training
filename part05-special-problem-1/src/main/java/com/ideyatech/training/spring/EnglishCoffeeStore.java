package com.ideyatech.training.spring;

import com.ideyatech.training.spring.xmlconfig.CoffeeStore;

/**
 * Created by USER on 5/29/2017.
 */
//TODO 1. Create an implementation for EnglishCoffeeStore
public interface EnglishCoffeeStore extends CoffeeStore {
    //TODO 2. Read a message property with English locale, property name is coffee
}
