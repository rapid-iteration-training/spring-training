package com.ideyatech.training.spring.polymorphism;

/**
 * Created by USER on 5/29/2017.
 */
public interface Shape {
    void draw();
}
