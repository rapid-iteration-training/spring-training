package com.ideyatech.training.spring.polymorphism;

/**
 * Created by USER on 5/29/2017.
 */
public class ShapeApplication {
    public static void main(String[] args) {
        ShapeDrawer shapeDrawer = new ShapeDrawer();

        shapeDrawer.drawShape(new Circle());

        shapeDrawer.drawShape(new Triangle());

        shapeDrawer.drawShape(new Square());
    }
}
