package com.ideyatech.training.spring.badcode;

/**
 * Created by USER on 5/29/2017.
 */
public class ShapeDrawer {
    public void draw(Object object) {
        if(object instanceof Circle) {
            ((Circle)object).drawCircle();
        } else if(object instanceof Square) {
            ((Square)object).drawSquare();
        } else if(object instanceof Triangle) {
            ((Triangle)object).drawTriangle();
        }
    }
}
