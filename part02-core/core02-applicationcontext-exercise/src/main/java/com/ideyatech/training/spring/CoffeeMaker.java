package com.ideyatech.training.spring;

/**
 * Created by USER on 5/29/2017.
 */
//TODO 1. Create Implementation for CoffeeMaker and use System.out.println to output some text
public interface CoffeeMaker {
    Coffee prepare();
}
