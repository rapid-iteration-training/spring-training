package com.ideyatech.training.spring;

/**
 * Created by USER on 5/29/2017.
 */
public class Coffee {
    private String output;

    public Coffee(String output) {
        this.output = output;
    }

    public void drink() {
        System.out.println(output);
    }
}
