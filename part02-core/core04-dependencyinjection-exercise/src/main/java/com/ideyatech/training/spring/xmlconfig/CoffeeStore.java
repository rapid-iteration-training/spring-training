package com.ideyatech.training.spring.xmlconfig;

import com.ideyatech.training.spring.Coffee;

/**
 * Created by USER on 5/29/2017.
 */
//TODO 1. Create a coffee store with Constructor DependencyInjection of a CoffeeMaker
public interface CoffeeStore {
    Coffee buy();
}
