package com.ideyatech.training.spring.xmlconfig;

import com.ideyatech.training.spring.Forex;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by USER on 5/29/2017.
 */
public class PesoForex implements Forex {
    private Map<Currency, BigDecimal> currencyMap = new HashMap<Currency, BigDecimal>();

    public PesoForex() {
        this.currencyMap.put(Currency.getInstance(Locale.US), BigDecimal.valueOf(50));

        this.currencyMap.put(Currency.getInstance(Locale.UK), BigDecimal.valueOf(60));
    }

    public BigDecimal getRate(Currency currency) {
        return currencyMap.get(currency);
    }
}
