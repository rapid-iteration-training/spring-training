package com.ideyatech.training.spring.xmlconfig;

import com.ideyatech.training.spring.Forex;
import com.ideyatech.training.spring.MoneyChanger;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by USER on 5/29/2017.
 */
public class PesoMoneyChanger implements MoneyChanger {
    private Forex forex;

    public PesoMoneyChanger(Forex forex) {
        this.forex = forex;
    }



    public BigDecimal convert(BigDecimal currencyAmount, Currency fromCurrency) {
        return forex.getRate(fromCurrency).multiply(currencyAmount);
    }

/*   */
    public PesoMoneyChanger() {
    }
    public void setForex(Forex forex) {
        this.forex = forex;
    }
}
