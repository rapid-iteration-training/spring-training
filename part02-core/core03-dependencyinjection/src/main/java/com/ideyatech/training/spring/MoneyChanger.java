package com.ideyatech.training.spring;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Created by USER on 5/29/2017.
 */
public interface MoneyChanger {
    BigDecimal convert(BigDecimal currencyAmount, Currency fromCurrency);
}
