package com.ideyatech.training.spring.xmlconfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplicationXmlConfig {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        PesoMoneyChanger pesoMoneyChanger = applicationContext.getBean(PesoMoneyChanger.class);
        BigDecimal dollarAmount = BigDecimal.valueOf(100);
        BigDecimal convertedPesoFromDollar = pesoMoneyChanger.convert(dollarAmount, Currency.getInstance(Locale.US));
        System.out.println(dollarAmount + " USD is amounting to Php " + convertedPesoFromDollar);
    }
}
