package com.ideyatech.training.spring.xmlconfig;

import com.ideyatech.training.spring.Coffee;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplicationXmlConfig {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        CoffeeStore coffeeStore = applicationContext.getBean(CoffeeStore.class);
        Coffee coffee = coffeeStore.buy();
        coffee.drink();
    }
}
