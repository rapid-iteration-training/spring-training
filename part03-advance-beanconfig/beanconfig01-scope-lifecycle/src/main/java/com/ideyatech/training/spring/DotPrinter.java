package com.ideyatech.training.spring;

/**
 * Created by USER on 5/29/2017.
 */
public class DotPrinter implements Printer {
    private String textToPrint = "Printing Successful!";

    public DotPrinter() {
        System.out.println("Constructor called");
    }

    public void init() {
        System.out.println("Initializing Printer");
    }

    public void print() {
        System.out.println(textToPrint);
    }

    public void destroy() {
        System.out.println("Destroying Printer");
    }

}
