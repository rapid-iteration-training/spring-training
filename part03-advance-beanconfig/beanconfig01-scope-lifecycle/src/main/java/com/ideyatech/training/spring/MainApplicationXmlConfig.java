package com.ideyatech.training.spring;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplicationXmlConfig {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");

        applicationContext.getBean("dotPrinter", Printer.class).print();

        applicationContext.getBean("dotPrinter", Printer.class).print();

        //FORCING to destroy bean
        ((ConfigurableApplicationContext) applicationContext).close();
    }
}
