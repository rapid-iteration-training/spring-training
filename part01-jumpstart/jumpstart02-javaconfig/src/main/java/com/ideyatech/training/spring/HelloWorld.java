package com.ideyatech.training.spring;

import org.springframework.stereotype.Component;

/**
 * Created by USER on 5/28/2017.
 */
@Component
public class HelloWorld {

    public void sayHello() {
        System.out.println("Mabuhay Pilipinas!");
    }
}
