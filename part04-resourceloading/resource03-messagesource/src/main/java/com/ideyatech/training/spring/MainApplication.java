package com.ideyatech.training.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Locale;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplication {
    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("application-context.xml");
        String greetingChinese = applicationContext.getMessage("greeting", null, null, Locale.CHINA);
        System.out.println(greetingChinese);

        String greetingEnglish = applicationContext.getMessage("greeting", null, null, new Locale("EN"));
        System.out.println(greetingEnglish);

        String greetingTaglish = applicationContext.getMessage("greeting", null, new Locale("FR"));
        System.out.println(greetingTaglish);
    }
}
