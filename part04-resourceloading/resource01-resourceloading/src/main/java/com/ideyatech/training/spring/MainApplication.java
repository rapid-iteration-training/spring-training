package com.ideyatech.training.spring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by USER on 5/29/2017.
 */
public class MainApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new GenericApplicationContext();
        Resource resource = applicationContext.getResource("classpath:input.txt");
        try{
            InputStream is = resource.getInputStream();
            Scanner scanner = new Scanner(new InputStreamReader(is));

            System.out.println(scanner.next());

        }catch(IOException e){
            e.printStackTrace();
        }
    }
}
